import { TransactWriteItemsCommand } from '@aws-sdk/client-dynamodb';
import { dynamodb } from '@services/dynamodb.service';

const newChat = async (
  event: AWSLambda.DynamoDBStreamEvent,
  _context: AWSLambda.Context,
  callback: AWSLambda.Callback
): Promise<void> => {
  const transactionCommand = new TransactWriteItemsCommand({ TransactItems: [] });

  /**
   * TODO: add support to group chats
   */
  event.Records.forEach((record) => {
    if (record.dynamodb?.StreamViewType === 'NEW_IMAGE') {
      const user1Command = {
        Update: {
          TableName: 'user',
          Key: {
            username: { S: record.dynamodb.NewImage?.participants.L?.[0].S as string },
            sortKey: { S: 'chatList' },
          },
          UpdateExpression: 'SET #chatList = list_append(if_not_exists(#chatList, :emptyList), :list)',
          ExpressionAttributeValues: {
            ':list': {
              L: [{ S: record.dynamodb?.NewImage?.id.S as string }],
            },
            ':emptyList': {
              L: [],
            },
          },
          ExpressionAttributeNames: {
            '#chatList': 'chatList',
          },
        },
      };
      const user2Command = {
        Update: {
          TableName: 'user',
          Key: {
            username: { S: record.dynamodb.NewImage?.participants.L?.[1].S as string },
            sortKey: { S: 'chatList' },
          },
          UpdateExpression: 'SET #chatList = list_append(if_not_exists(#chatList, :emptyList), :list)',
          ExpressionAttributeValues: {
            ':list': {
              L: [{ S: record.dynamodb?.NewImage?.id.S as string }],
            },
            ':emptyList': {
              L: [],
            },
          },
          ExpressionAttributeNames: {
            '#chatList': 'chatList',
          },
        },
      };
      transactionCommand.input.TransactItems?.push(user1Command);
      transactionCommand.input.TransactItems?.push(user2Command);
    }
  });

  try {
    if (transactionCommand.input.TransactItems?.length) {
      await dynamodb.send(transactionCommand);
      console.info('updated records');
    } else {
      console.info('nothing to update');
    }
    callback(null);
  } catch (error: any) {
    console.error(error.message);
    callback(error);
  }
};

export { newChat };
