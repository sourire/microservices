import { findQueryByPk, postToConnection, putItem } from '@services';

import { Response } from '@utils/response';
import { newChatDTO } from '@dto/websocket/new-chat';
import { randomUUID } from 'crypto';

const newChat = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  const { requestContext, body } = event;
  const json = JSON.parse(body as string)?.payload;
  try {
    const body = await newChatDTO.validate(json);

    /**
     * TODO: validate if user exits and chat does not
     */
    const participantOnline = await findQueryByPk(
      { Key: { username: { S: body.participantId }, sortKey: { S: 'online' } } },
      'user'
    );

    const chatId = randomUUID();

    await putItem(
      {
        id: {
          S: chatId,
        },
        sortKey: {
          S: 'participants',
        },
        participants: {
          L: [{ S: requestContext.authorizer?.principalId }, { S: body.participantId }],
        },
        owner: {
          S: requestContext.authorizer?.principalId,
        },
      },
      'chat'
    );

    if (participantOnline.Item?.connectionId.S) {
      await postToConnection({
        ConnectionId: participantOnline.Item?.connectionId.S as string,
        Data: JSON.stringify(new Response({ message: 'ok', data: { chatId }, code: 0 }, 200)),
      });
      console.info('SENT');
    }

    return new Response({ message: 'ok', data: { chatId }, code: 0 }, 200);
  } catch (error: any) {
    console.info(error);
    return new Response({ message: error.message, code: 1 }, 500);
  }
};

export { newChat };
