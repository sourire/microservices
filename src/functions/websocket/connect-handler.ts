import { Response } from '@utils';
import { updateByPk } from '@services';

const connectHandler = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  /**
   * TODO: maybe set connection list as an array of connected devices
   */
  const updateCommand = (username: string, connectionId: string) => ({
    Key: {
      username: {
        S: username,
      },
      sortKey: {
        S: 'online',
      },
    },
    UpdateExpression: 'set connectionId = :online',
    ExpressionAttributeValues: {
      ':online': {
        S: connectionId,
      },
    },
  });
  if (event.requestContext.eventType === 'CONNECT') {
    try {
      await updateByPk(
        updateCommand(event.requestContext.authorizer?.principalId, event.requestContext.connectionId as string),
        'user'
      );
      return new Response({ message: 'ok', code: 0 }, 200);
    } catch (error: any) {
      console.error(error);

      return new Response({ message: 'ok', code: 0 }, 401);
    }
  } else {
    const username = event.requestContext.authorizer?.username;
    await updateByPk(updateCommand(username, ''), 'user');

    return new Response({ message: 'ok', code: 0 }, 200);
  }
};

export { connectHandler };
