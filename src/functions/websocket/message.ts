import { findQueryByPk, postToConnection, putItem } from '@services';

import { Response } from '@utils/response';
import { messageDTO } from '@dto/websocket/message';
import { randomUUID } from 'crypto';

const message = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  const { requestContext, body } = event;
  const json = JSON.parse(body as string)?.payload;
  try {
    const body = await messageDTO.validate(json);
    console.info(body);

    /**
     * TODO: validate if user exits and chat does not
     */
    const chat = await findQueryByPk({ Key: { id: { S: body.chatId }, sortKey: { S: 'participants' } } }, 'chat');

    const recipient = await findQueryByPk(
      {
        Key: {
          username: {
            S: chat.Item?.participants.L?.find((p) => p.S !== requestContext.authorizer?.principalId)?.S as string,
          },
          sortKey: {
            S: 'online',
          },
        },
      },
      'user'
    );

    await putItem(
      {
        chatId: {
          S: body.chatId,
        },
        id: {
          S: randomUUID(),
        },
        text: {
          S: body.text,
        },
        sender: {
          S: requestContext.authorizer?.principalId,
        },
        type: {
          S: 'text',
        },
        updatedAt: {
          N: `${Date.now()}`,
        },
        createdAt: {
          N: `${Date.now()}`,
        },
      },
      'message'
    );

    console.info(recipient.Item);
    if (recipient.Item?.connectionId?.S) {
      await postToConnection({
        ConnectionId: recipient.Item?.connectionId?.S as string,
        Data: JSON.stringify(
          new Response(
            {
              message: 'ok',
              data: { chatId: body.chatId, sender: requestContext.authorizer?.principalId, text: body.text },
              code: 0,
            },
            200
          )
        ),
      });
    }

    return new Response({ message: 'ok', data: {}, code: 0 }, 200);
  } catch (error: any) {
    console.info(error);
    return new Response({ message: error.message, code: 1 }, 500);
  }
};

export { message };
