import { Response } from '@utils';
import { getUser } from '@services';

const websocketAuthorizer = async (
  event: AWSLambda.APIGatewayAuthorizerEvent,
  callback: AWSLambda.Callback
): Promise<Response> => {
  if (event.type === 'REQUEST') {
    const accessToken = event.queryStringParameters?.['access-token'] || '';
    try {
      const user = await getUser({ AccessToken: accessToken });
      const response = new Response({ message: 'ok', code: 0 }, 200);

      callback(null, {
        principalId: user.Username,
        policyDocument: {
          Version: '2012-10-17',
          Statement: [{ Action: 'execute-api:Invoke', Effect: 'Allow', Resource: (event as any).methodArn }],
        },
        context: {
          username: user.Username,
        },
      });

      return response;
    } catch (error: any) {
      console.error(`user not authorized, reason - [${error.message}]`);
      console.error(error);

      /**
       * TODO: document errors
       */
      const response = new Response(
        {
          message: error.message,
          code: 1,
        },
        500
      );
      return response;
    }
  }
  console.error(`Unsupported type - [${event.type}]`);
  const response = new Response(
    {
      message: 'unsupported authorizer type',
      code: 1,
    },
    500
  );
  return response;
};

export { websocketAuthorizer };
