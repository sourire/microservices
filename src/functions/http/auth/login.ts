import { Response } from '@utils';
import environment from '@env';
import { initiateAuth } from '@services';
import { loginDTO } from '@dto';

const login = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  try {
    const body = loginDTO.validateSync(JSON.parse(event.body as string));

    const result = await initiateAuth({
      AuthFlow: 'USER_PASSWORD_AUTH',
      AuthParameters: {
        USERNAME: body.email,
        PASSWORD: body.password,
      },
      ClientId: environment.cognito.clientId,
    });

    return new Response(
      {
        message: 'ok',
        code: 0,
        data: {
          access: result.AuthenticationResult?.AccessToken,
          refresh: result.AuthenticationResult?.RefreshToken,
          expiresIn: result.AuthenticationResult?.ExpiresIn,
        },
      },
      200
    );
  } catch (error: any) {
    if (error.name === 'UserNotConfirmedException') {
      return new Response({ message: error.name, code: 1 }, error.statusCode);
    }
    if (error.name === 'NotAuthorizedException') {
      return new Response({ message: error.name, code: 1 }, error.statusCode);
    }

    return new Response({ message: error.message, code: 1 }, 500);
  }
};

export { login };
