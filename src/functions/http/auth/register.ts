import { putItem, signUp } from '@services';

import { Response } from '@utils';
import environment from '@env';
import { registerDTO } from '@dto';

/**
 * Registers the user in AWS Cognito service
 */
const register = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  try {
    const body = await registerDTO.validate(JSON.parse(event.body as string));

    await signUp({
      ClientId: environment.cognito.clientId,
      Username: body.username,
      Password: body.password,
      UserAttributes: [{ Name: 'email', Value: body.email }],
    });
    const response = new Response({ message: 'ok', code: 0 }, 201);
    return response;
  } catch (error) {
    const response = new Response(
      {
        message: (error as Error).message,
        code: 1,
      },
      500
    );

    return response;
  }
};

/**
 * PostConfirmation trigger
 * Saves user to the DynamoDB
 * TODO: move this out and rename
 */
const postRegister = async (
  event: AWSLambda.PostConfirmationTriggerEvent,
  _context: AWSLambda.Context,
  callback: AWSLambda.Callback
): Promise<void> => {
  try {
    const userSettings = {
      email: event.request.userAttributes.email,
      updatedAt: Date.now(),
      createdAt: Date.now(),
    };
    const newUserAttributes = {
      username: {
        S: event.userName,
      },
      sortKey: {
        S: `user_${JSON.stringify(userSettings)}`,
      },
    };

    await putItem(newUserAttributes, 'user');

    console.info('Created user');

    callback(null, event);
  } catch (error: any) {
    console.error(`Failed to create user, reason - [${error.message}]`);
    callback(null, event);
  }
};

export { register, postRegister };
