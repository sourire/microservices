import { Response } from '@utils';
import { getUser } from '@services';

const auth = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  try {
    const accessToken = event.headers['access-token'] || '';

    const user = await getUser({ AccessToken: accessToken });

    const response = new Response({ message: 'ok', code: 0, data: user }, 200);

    // callback(null, {
    //   principalId: user.Username,
    //   policyDocument: {
    //     Version: '2012-10-17',
    //     Statement: [{ Action: 'execute-api:Invoke', Effect: 'Allow', Resource: (event as any).methodArn }],
    //   },
    //   context: {
    //     username: user.Username,
    //   },
    // });

    return response;
  } catch (error: any) {
    const response = new Response(
      {
        message: (error as Error).message,
        code: 1,
      },
      500
    );

    console.info(`user not authorized, reason - [${error.message}]`);

    return response;
  }
};

export { auth };
