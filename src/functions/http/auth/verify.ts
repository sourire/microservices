import { resendConfirmationCode, verifyUser } from '@services';
import { resendDTO, verifyUserDTO } from '@dto';

import { Response } from '@utils';
import environment from '@environment';

const verify = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  try {
    const body = await verifyUserDTO.validate(JSON.parse(event.body as string));

    await verifyUser({
      ClientId: environment.cognito.clientId,
      ConfirmationCode: body.code,
      Username: body.username,
    });

    const response = new Response({ message: 'ok', code: 0 }, 200);

    return response;
  } catch (error) {
    const response = new Response(
      {
        message: (error as Error).message,
        code: 1,
      },
      500
    );

    return response;
  }
};

const resend = async (event: AWSLambda.APIGatewayEvent): Promise<Response> => {
  try {
    const body = await resendDTO.validate(JSON.parse(event.body as string));

    await resendConfirmationCode({ ClientId: environment.cognito.clientId, Username: body.username });

    const response = new Response({ message: 'ok', code: 0 }, 200);

    return response;
  } catch (error) {
    const response = new Response(
      {
        message: (error as Error).message,
        code: 1,
      },
      500
    );

    return response;
  }
};

export { verify, resend };
