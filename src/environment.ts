const environment = {
  mode: process.env['NODE_ENV'] as 'dev' | 'local',
  aws: {
    region: process.env['AWS_REGION_LOCAL'] as string,
  },
  cognito: {
    clientId: process.env['AWS_CLIENT_ID'] as string,
  },
};

export default environment;
