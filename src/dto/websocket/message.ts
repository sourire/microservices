import { object, string } from 'yup';

const messageDTO = object().shape({
  text: string().required(),
  chatId: string().uuid().required(),
});

export { messageDTO };
