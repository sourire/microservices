import { object, string } from 'yup';

const newChatDTO = object().shape({
  participantId: string().required(),
});

export { newChatDTO };
