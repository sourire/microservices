import { object, string } from 'yup';

const textMessageDTO = object().shape({
  chatId: string().uuid().required(),
  text: string().required(),
});

export { textMessageDTO };
