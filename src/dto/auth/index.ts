import { object, string } from 'yup';

const registerDTO = object().shape({
  password: string()
    .min(6)
    .matches(/(?=.*\d)(?=.*[a-z])(?=.*[\^\$\*\.\[\]\{\}\(\)\?\-\"\!\@\#\%\&\/\\\,\>\<\'\:\;\|\_\~\`\+\=]).*/)
    .required(),
  email: string().email().required(),
  username: string().min(4).max(256).required(),
});

const verifyUserDTO = object().shape({
  username: string().required(),
  code: string()
    .matches(/^[0-9]+$/)
    .length(6)
    .required(),
});

const loginDTO = object().shape({
  email: string().email().required(),
  password: string().required(),
});

const resendDTO = object().shape({
  username: string().required(),
});

export { registerDTO, verifyUserDTO, loginDTO, resendDTO };
