import { object, string } from 'yup';

const searchUserDTO = object().shape({
  search: string().required(),
});

export { searchUserDTO };
