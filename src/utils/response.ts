type Body = {
  message: string;
  code: number;
  data?: unknown;
};

class Response {
  public body: string;

  public statusCode: number;

  public headers: Record<string, string>;

  constructor(body: Body, status: number, headers?: Record<string, string>) {
    this.body = JSON.stringify(body);
    this.statusCode = status;
    this.headers = {
      'Access-Control-Allow-Origin': 'http://localhost:3000',
      'Access-Control-Allow-Credentials': 'true',
      ...headers,
    };
  }
}

export { Response };
