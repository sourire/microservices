import {
  AttributeValue,
  BatchWriteItemCommand,
  BatchWriteItemCommandOutput,
  DynamoDBClient,
  GetItemCommand,
  GetItemCommandInput,
  GetItemCommandOutput,
  PutItemCommand,
  PutItemCommandOutput,
  ScanCommand,
  ScanCommandInput,
  ScanCommandOutput,
  UpdateItemCommand,
  UpdateItemCommandInput,
  UpdateItemCommandOutput,
  WriteRequest,
} from '@aws-sdk/client-dynamodb';

import { Tables } from '@models';
import environment from '@env';

/**
 * TODO: Define better database access layer with typed tables and general methods
 */
const dynamodb = new DynamoDBClient({ region: environment.aws.region });

const putItem = (item: Record<string, AttributeValue> | undefined, table: Tables): Promise<PutItemCommandOutput> => {
  const putItemCommand = new PutItemCommand({
    TableName: table,
    Item: item,
  });
  return dynamodb.send(putItemCommand);
};

const findQueryByPk = (
  command: Omit<GetItemCommandInput, 'TableName'>,
  table: Tables
): Promise<GetItemCommandOutput> => {
  const getItemCommand = new GetItemCommand({
    TableName: table,
    ...command,
  });

  return dynamodb.send(getItemCommand);
};

const scanTable = (command: Omit<ScanCommandInput, 'TableName'>, table: Tables): Promise<ScanCommandOutput> => {
  const scanCommand = new ScanCommand({
    TableName: table,
    ...command,
  });

  return dynamodb.send(scanCommand);
};

const batchWriteItem = (command: WriteRequest[], table: Tables): Promise<BatchWriteItemCommandOutput> => {
  const batchWriteItemCommand = new BatchWriteItemCommand({
    RequestItems: {
      [table]: command,
    },
  });

  return dynamodb.send(batchWriteItemCommand);
};

const updateByPk = (
  command: Omit<UpdateItemCommandInput, 'TableName'>,
  table: Tables
): Promise<UpdateItemCommandOutput> => {
  const updateItemCommand = new UpdateItemCommand({
    TableName: table,
    ...command,
  });

  return dynamodb.send(updateItemCommand);
};

export { putItem, findQueryByPk, scanTable, batchWriteItem, updateByPk, dynamodb };
