import { ApiGatewayManagementApi } from 'aws-sdk';
import { PostToConnectionRequest } from 'aws-sdk/clients/apigatewaymanagementapi';
import environment from '@env';

const apiGateway = new ApiGatewayManagementApi({
  region: environment.aws.region,
  endpoint: 'http://localhost:3001',
});

const postToConnection = (params: PostToConnectionRequest): Promise<unknown> => {
  const promise = new Promise((res, rej) => {
    apiGateway.postToConnection(params, (error, data) => {
      console.info('sd');
      if (error) {
        return rej(error);
      }
      return res(data);
    });
  });

  return promise;
};

export { apiGateway, postToConnection };
