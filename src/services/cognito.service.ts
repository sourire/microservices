import Cognito from 'aws-sdk/clients/cognitoidentityserviceprovider';
import environment from 'src/environment';

const cognito = new Cognito({ region: environment.aws.region });

const signUp = (params: Cognito.SignUpRequest): Promise<Cognito.SignUpResponse> => {
  const promise = new Promise<Cognito.SignUpResponse>((res, rej) => {
    cognito.signUp(params, (err, data) => {
      if (err) {
        return rej(err);
      }

      return res(data);
    });
  });

  return promise;
};

const deleteUser = (params: Cognito.DeleteUserRequest): Promise<unknown> => {
  const promise = new Promise((res, rej) => {
    cognito.deleteUser(params, (err, data) => {
      if (err) {
        return rej(err);
      }

      return res(data);
    });
  });

  return promise;
};

const verifyUser = (params: Cognito.ConfirmSignUpRequest): Promise<Cognito.ConfirmSignUpResponse> => {
  const promise = new Promise<Cognito.ConfirmSignUpResponse>((res, rej) => {
    cognito.confirmSignUp(params, (error, data) => {
      if (error) {
        return rej(error);
      }

      return res(data);
    });
  });

  return promise;
};

const getUser = (params: Cognito.GetUserRequest): Promise<Cognito.GetUserResponse> => {
  const promise = new Promise<Cognito.GetUserResponse>((res, rej) => {
    cognito.getUser(params, (error, data) => {
      if (error) {
        return rej(error);
      }

      return res(data);
    });
  });

  return promise;
};

const initiateAuth = (params: Cognito.InitiateAuthRequest): Promise<Cognito.InitiateAuthResponse> => {
  const promise = new Promise<Cognito.InitiateAuthResponse>((res, rej) => {
    cognito.initiateAuth(params, (error, data) => {
      if (error) {
        return rej(error);
      }

      return res(data);
    });
  });

  return promise;
};

const resendConfirmationCode = (
  params: Cognito.ResendConfirmationCodeRequest
): Promise<Cognito.ResendConfirmationCodeResponse> => {
  const promise = new Promise<Cognito.ResendConfirmationCodeResponse>((res, rej) => {
    cognito.resendConfirmationCode(params, (error, data) => {
      if (error) {
        return rej(error);
      }

      return res(data);
    });
  });

  return promise;
};

export { signUp, deleteUser, verifyUser, getUser, initiateAuth, resendConfirmationCode };
