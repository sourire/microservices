const { readdirSync, writeFileSync, readFileSync } = require('fs');
const { lstat } = require('fs/promises');
const path = require('path');
const prettier = require('prettier');
const json5 = require('json5');

const setup = async () => {
  try {
    const tsconfig = json5.parse(readFileSync(path.join(process.cwd(), 'tsconfig.json')));

    const modules = readdirSync(path.join(process.cwd(), 'src'));

    for (const m of modules) {
      const stats = await lstat(path.join(process.cwd(), 'src', m));

      if (stats.isFile()) {
        const names = m.split('.');
        names.pop();
        const fileName = names.join();

        tsconfig.compilerOptions.paths[`@${fileName}`] = [`src/${m}`];
      } else {
        tsconfig.compilerOptions.paths[`@${m}`] = [`src/${m}`];
        tsconfig.compilerOptions.paths[`@${m}/*`] = [`src/${m}/*`];
      }
    }

    const formatted = prettier.format(JSON.stringify(tsconfig), { parser: 'json' });

    writeFileSync(path.join(process.cwd(), 'tsconfig.json'), formatted);

    console.info(`Updated [${modules.length}] module paths`);
  } catch (error) {
    console.info(`Failed to update tsconfig paths, reason - [${error.message}]`);
  }
};

setup();
